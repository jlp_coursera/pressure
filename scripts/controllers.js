'use strict';

angular.module('pressureApp')

        .controller('ProductsController', ['$scope', '$sce', 'productsFactory', function($scope, $sce, productsFactory) {

            $scope.products= productsFactory.getProducts().query();
			$scope.tips= productsFactory.getTips().query();
			console.log($scope.products);
            $scope.select = function(setTab) {
                if (setTab === 1) {
                    $scope.filtText = "";
                }
                else if (setTab === 2) {
				    $scope.filtText = "shirts";
                }
                else if (setTab === 3) {
                    $scope.filtText = "pants";
                }
                else if (setTab === 4) {
                    $scope.filtText = "decks";
                }
                else if (setTab === 5) {
                    $scope.filtText = "trucks";
                }
                else if (setTab === 6) {
                    $scope.filtText = "wheels";
                }
                else {
                    $scope.filtText = "";
                }
            };
			
			$scope.getTipUrl = function(tip) {
				return $sce.trustAsResourceUrl(tip.video);
			};
        }])
		
		.controller('CartController', ['$scope', 'productsFactory', function($scope, productsFactory) {
            var i;
			var j;
			var bFound = 0;
			$scope.cartProducts = [];
            $scope.products= productsFactory.getProducts().query();
            $scope.cart= productsFactory.getCart().query();
			$scope.total = 0;
			
			for (i = 0; i < $scope.cart.length; i++) { 
			    /* Find if already pushed*/
				for (j=0; j< $scope.cartProducts.length; j++){
					if ($scope.cartProducts[j]._id === $scope.cart[i]){
						bFound = 1;
						break;
					}
				}
				if (bFound){
					$scope.cartProducts[j].times++;
					$scope.total = $scope.total + parseFloat($scope.cartProducts[j].price,10);
					bFound = 0;
				} else {
					$scope.cartProducts.push(productsFactory.getProducts().get({id:$scope.cart[i]}));
					$scope.cartProducts[$scope.cartProducts.length-1].times = 1;
					$scope.total = $scope.total + parseFloat($scope.cartProducts[$scope.cartProducts.length-1].price,10);
				}
			}
			
			
			$scope.total = Number(($scope.total).toFixed(2));
			
			$scope.buy = function() {
				$scope.cartProducts = [];
				$scope.cart = [];
				$scope.total = 0;
				return;
			};
        }])
		
		.controller('WishesController', ['$scope', 'productsFactory', function($scope, productsFactory) {
            var i;
			var product;
			
			$scope.wishesProducts = [];
            $scope.products= productsFactory.getProducts().query();
            $scope.wishes= productsFactory.getWishes().query();
			
			
			
			
			
			for (i = 0; i < $scope.wishes.length; i++) { 
			  product = productsFactory.getProducts().get({id:$scope.wishes[i]}).$promise.then(
                            function(response){
                                product = response;
                            },
                            function(response) {
                                $scope.message = "Error: "+response.status + " " + response.statusText;
                            }
            );
			
				$scope.wishesProducts.push(product);
			}
			
			
        }])
		
		.controller('WorkshopController', ['$scope', 'productsFactory', function($scope, productsFactory) {
            var i;
			
            $scope.products= productsFactory.getProducts().query();
			
			
			
			$scope.selectDeck = function(product) {
				$scope.deck = product;
				return;
			};
			$scope.selectTrucks = function(product) {
				$scope.trucks = product;
				return;
			};
			$scope.selectWheels = function(product) {
				$scope.wheels = product;
				return;
			};
			
			$scope.addToCart = function(){
				$scope.cart= productsFactory.getCart().query();
				$scope.cart.push($scope.deck._id);
				$scope.cart.push($scope.trucks._id);
				$scope.cart.push($scope.wheels._id);
				
			};
			
			$scope.addToWishes = function(){
				$scope.wishes= productsFactory.getWishes().query();
				for (i=0; i< $scope.wishes.length; i++){
					if ($scope.wishes[i] === $scope.deck._id){
						return;
					}
				}
				$scope.wishes.push($scope.deck._id);
				for (i=0; i< $scope.wishes.length; i++){
					if ($scope.wishes[i] === $scope.trucks._id){
						return;
					}
				}
				$scope.wishes.push($scope.trucks._id);
				for (i=0; i< $scope.wishes.length; i++){
					if ($scope.wishes[i] === $scope.wheels._id){
						return;
					}
				}
				$scope.wishes.push($scope.wheels._id);
			};
        }])	
		
        .controller('ContactController', ['$scope', function($scope) {

            $scope.feedback = {mychannel:"", firstName:"", lastName:"", agree:false, email:"" };
            
            var channels = [{value:"tel", label:"Tel."}, {value:"Email",label:"Email"}];
            
            $scope.channels = channels;
            $scope.invalidChannelSelection = false;           
        }])

        .controller('FeedbackController', ['$scope', function($scope) {
            $scope.sendFeedback = function() {
                
                console.log($scope.feedback);
                
                if ($scope.feedback.agree && ($scope.feedback.mychannel === "")) {
                    $scope.invalidChannelSelection = true;
                    console.log('incorrect');
                }
                else {
                    $scope.invalidChannelSelection = false;
                    $scope.feedback = {mychannel:"", firstName:"", lastName:"", agree:false, email:"" };
                    $scope.feedback.mychannel="";
                    $scope.feedbackForm.$setPristine();
                    console.log($scope.feedback);
                }
            };
        }])

        .controller('ProductDetailController', ['$scope', '$stateParams', 'productsFactory', function($scope, $stateParams, productsFactory) {
            var i;
			$scope.product = {};
			
            $scope.product = productsFactory.getProducts().get({
				id:$stateParams.id
				})
			.$promise.then(
                            function(response){
                                $scope.product = response;
                            },
                            function(response) {
                                $scope.message = "Error: "+response.status + " " + response.statusText;
                            }
            );
	 		
			$scope.addToCart = function(){
				$scope.cart = productsFactory.getCart().query();
				$scope.cart.products.push($scope.product._id);
				productsFactory.getCart().save($scope.product._id);
			};
			
			$scope.addToWishes = function(){
				$scope.wishes = productsFactory.getWishes().query();
				for (i=0; i< $scope.wishes.length; i++){
					if ($scope.wishes[i] === $scope.product._id){
						return;
					}
				}
				$scope.wishes.push($scope.product.id);
			};
        }])

				
        .controller('ProductCommentController', ['$scope', function($scope) {
            
            $scope.formComment = {rating:5, comment:"", author:"", date:""};
            
            $scope.submitComment = function () {
                
                $scope.formComment.date = new Date().toISOString();
                console.log($scope.formComment);
                
                $scope.product.comments.push($scope.formComment);
                
                $scope.commentForm.$setPristine();
                
                $scope.formComment = {rating:5, comment:"", author:"", date:""};
            };
        }])


;
