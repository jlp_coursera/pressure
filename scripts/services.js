'use strict';

angular.module('pressureApp')
        .constant("baseURL","https://pressureflipshop-monohydrated-majolica.eu-gb.mybluemix.net/")
        .service('productsFactory', ['$resource','baseURL', function($resource,baseURL) {
    
			this.getProducts = function(){
				
				return $resource(baseURL + "products/:id", null, {'update':{method:'PUT' }});
				
			};

			this.getTips = function(){
				
				return $resource(baseURL + "tips/:id", null, {'update':{method:'PUT' }});
				
			};
			
			this.getCart = function(){
				
				return $resource(baseURL + "cart/:id", null, {'update':{method:'PUT' }});
			
			};
	
			this.getWishes = function(){
				
				return $resource(baseURL + "wishes/:id", null, {'update':{method:'PUT' }});
				
			};

        }])


;
