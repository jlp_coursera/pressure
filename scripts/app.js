'use strict';

angular.module('pressureApp',['ui.router','ngResource'])
.config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            // route for the home page
            .state('app', {
                url:'/',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                    },
                    'content': {
                        templateUrl : 'views/home.html',
                        controller  : 'ProductsController'
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                }

            })
        
            // route for the contactus page
            .state('app.contactus', {
                url:'contactus',
                views: {
                    'content@': {
                        templateUrl : 'views/contactus.html',
                        controller  : 'ContactController'                  
                    }
                }
            })

            // route for the clothing page
            .state('app.clothing', {
                url: 'clothing',
                views: {
                    'content@': {
                        templateUrl : 'views/clothing.html',
                        controller  : 'ProductsController'
                    }
                }
            })
			
            // route for the workshop page
            .state('app.workshop', {
                url: 'workshop',
                views: {
                    'content@': {
                        templateUrl : 'views/workshop.html',
                        controller  : 'WorkshopController'
                    }
                }
            })
			
            // route for the tips page
            .state('app.tips', {
                url: 'tips',
                views: {
                    'content@': {
                        templateUrl : 'views/tips.html',
                        controller  : 'ProductsController'
                    }
                }
            })

            // route for the productdetail page
            .state('app.productdetails', {
                url: 'products/:id',
                views: {
                    'content@': {
                        templateUrl : 'views/productdetail.html',
                        controller  : 'ProductDetailController'
                   }
                }
            })
			
            // route for the cart page
            .state('app.cart', {
                url: 'cart',
                views: {
                    'content@': {
                        templateUrl : 'views/cart.html',
                        controller  : 'CartController'
                   }
                }
            })
			
            // route for the wishes page
            .state('app.wishes', {
                url: 'wishes',
                views: {
                    'content@': {
                        templateUrl : 'views/wishes.html',
                        controller  : 'WishesController'
                   }
                }
            });
    
        $urlRouterProvider.otherwise('/');
    })
;
