// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
require('mongoose-currency').loadType(mongoose);
var Currency = mongoose.Types.Currency;

var commentSchema = new Schema({
    rating:  {
        type: Number,
        min: 1,
        max: 5,
        required: true
    },
    comment:  {
        type: String,
        required: true
    },
    postedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: true
});

var productSchema = new Schema({
	_id:  {
		type:Schema.Types.ObjectId
	},
	id:  {
		type:Number
	},
    name: {
		type: String, 
		required: true, 
		unique: true 
	},
    image: {
		type: String,
		default: ''
	},
    imagews: {
		type: String,
		default: ''
	},
    category: {
		type: String, 
		default: ''
	},
    label: {
		type: String, 
		default: ''
	},
    price: {
		type: Currency, 
		default: ''
	},
    description: {
		type: String, 
		required: true
	},
    comments: [commentSchema] 
}, {
    timestamps: true
});

// the schema is useless so far
// we need to create a model using it
var Products = mongoose.model('Product', productSchema);

// make this available to our Node applications
module.exports = Products;