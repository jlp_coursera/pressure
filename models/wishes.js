// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var wishesSchema = new Schema({
	_id:  {
		type:Schema.Types.ObjectId
	},
	
	products: [
	{
		type:Schema.Types.ObjectId
	}
	
	]
}, {
    timestamps: true
});

// the schema is useless so far
// we need to create a model using it
var Wishes = mongoose.model('Wishes', wishesSchema);

// make this available to our Node applications
module.exports = Wishes;