// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tipSchema = new Schema({
	_id:  {
		type:Schema.Types.ObjectId
	},
	name: {
		type: String, 
		required: true, 
		unique: true 
	},
    image: {
		type: String,
		default: ''
	},
    category: {
		type: String, 
		default: ''
	},

    description: {
		type: String, 
		required: true
	},
    video: {
		type: String, 
		required: true
	},
}, {
    timestamps: true
});

// the schema is useless so far
// we need to create a model using it
var Tips = mongoose.model('Tip', tipSchema);

// make this available to our Node applications
module.exports = Tips;