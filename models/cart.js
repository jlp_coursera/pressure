// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var cartSchema = new Schema({
	_id:  {
		type:Schema.Types.ObjectId
	},
	
	products: [
	{
		type:Schema.Types.ObjectId
	}
	
	]
}, {
    timestamps: true
});

// the schema is useless so far
// we need to create a model using it
var Cart = mongoose.model('Cart', cartSchema);

// make this available to our Node applications
module.exports = Cart;