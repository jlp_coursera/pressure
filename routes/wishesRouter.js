var bodyParser = require('body-parser');
var express = require('express');
var mongoose = require('mongoose');

var wishesRouter = express.Router();

var Wishes = require('../models/wishes');

var Verify = require('./verify');

wishesRouter.use(bodyParser.json());

wishesRouter.route('/')
//Verify.verifyOrdinaryUser, 
.get(function (req, res, next) {
    Wishes.find({})
        .populate('comments.postedBy')
        .exec(function (err, wishes) {
        if (err) throw err;
        res.json(wishes);
    });
})

.post(function (req, res, next) {
    Wishes.create(req.body, function (err, wishes) {
        if (err) throw err;
        console.log('Wishes created!');
        var id = wishes._id;
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });

        res.end('Added the wishes with id: ' + id);
    });
})

.delete(function (req, res, next) {
    Wishes.remove({}, function (err, resp) {
        if (err) throw err;
        res.json(resp);
    });
});

wishesRouter.route('/:wishesId')
.get(function (req, res, next) {
    Wishes.findById(req.params.wishesId)
        .populate('comments.postedBy')
        .exec(function (err, wishes) {
        if (err) throw err;
        res.json(wishes);
    });
})

.put(function (req, res, next) {
    Wishes.findByIdAndUpdate(req.params.wishesId, {
        $set: req.body
    }, {
        new: true
    }, function (err, wishes) {
        if (err) throw err;
        res.json(wishes);
    });
})

.delete(function (req, res, next) {
        Wishes.findByIdAndRemove(req.params.wishesId, function (err, resp) {
        if (err) throw err;
        res.json(resp);
    });
});


module.exports = wishesRouter;