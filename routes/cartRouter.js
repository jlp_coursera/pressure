var bodyParser = require('body-parser');
var express = require('express');
var mongoose = require('mongoose');

var cartRouter = express.Router();

var Cart = require('../models/cart');

var Verify = require('./verify');

cartRouter.use(bodyParser.json());

cartRouter.route('/')
//Verify.verifyOrdinaryUser, 
.get(function (req, res, next) {
    Cart.find({})
        .populate('comments.postedBy')
        .exec(function (err, cart) {
        if (err) throw err;
        res.json(cart);
    });
})

.post(function (req, res, next) {
    Cart.create(req.body, function (err, cart) {
        if (err) throw err;
        console.log('Cart created!');
        var id = cart._id;
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });

        res.end('Added the cart with id: ' + id);
    });
})

.delete(function (req, res, next) {
    Cart.remove({}, function (err, resp) {
        if (err) throw err;
        res.json(resp);
    });
});

cartRouter.route('/:cartId')
.get(function (req, res, next) {
    Cart.findById(req.params.cartId)
        .populate('comments.postedBy')
        .exec(function (err, cart) {
        if (err) throw err;
        res.json(cart);
    });
})

.put(function (req, res, next) {
    Cart.findByIdAndUpdate(req.params.cartId, {
        $set: req.body
    }, {
        new: true
    }, function (err, cart) {
        if (err) throw err;
        res.json(cart);
    });
})

.delete(function (req, res, next) {
        Cart.findByIdAndRemove(req.params.cartId, function (err, resp) {
        if (err) throw err;
        res.json(resp);
    });
});


module.exports = cartRouter;