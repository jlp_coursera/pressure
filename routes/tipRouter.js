var bodyParser = require('body-parser');
var express = require('express');
var mongoose = require('mongoose');

var tipRouter = express.Router();

var Tip = require('../models/tips');

var Verify = require('./verify');

tipRouter.use(bodyParser.json());

tipRouter.route('/')
//Verify.verifyOrdinaryUser, 
.get(function (req, res, next) {
    Tip.find({})
        .populate('comments.postedBy')
        .exec(function (err, tip) {
        if (err) throw err;
        res.json(tip);
    });
})

.post(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req, res, next) {
    Tip.create(req.body, function (err, tip) {
        if (err) throw err;
        console.log('Tip created!');
        var id = tip._id;
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });

        res.end('Added the tip with id: ' + id);
    });
})

.delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req, res, next) {
    Tip.remove({}, function (err, resp) {
        if (err) throw err;
        res.json(resp);
    });
});

tipRouter.route('/:tipId')
.get(function (req, res, next) {
    Tip.findById(req.params.tipId)
        .populate('comments.postedBy')
        .exec(function (err, tip) {
        if (err) throw err;
        res.json(tip);
    });
})

.put(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req, res, next) {
    Tip.findByIdAndUpdate(req.params.tipId, {
        $set: req.body
    }, {
        new: true
    }, function (err, tip) {
        if (err) throw err;
        res.json(tip);
    });
})

.delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req, res, next) {
        Tip.findByIdAndRemove(req.params.tipId, function (err, resp) {
        if (err) throw err;
        res.json(resp);
    });
});



module.exports = tipRouter;