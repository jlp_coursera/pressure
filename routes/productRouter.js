var bodyParser = require('body-parser');
var express = require('express');
var mongoose = require('mongoose');

var productRouter = express.Router();

var Product = require('../models/products');

var Verify = require('./verify');

productRouter.use(bodyParser.json());

productRouter.route('/')
//Verify.verifyOrdinaryUser, 
.get(function (req, res, next) {
    Product.find({})
        .populate('comments.postedBy')
        .exec(function (err, product) {
        if (err) throw err;
        res.json(product);
    });
})

.post(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req, res, next) {
    Product.create(req.body, function (err, product) {
        if (err) throw err;
        console.log('Product created!');
        var id = product._id;
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });

        res.end('Added the product with id: ' + id);
    });
})

.delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req, res, next) {
    Product.remove({}, function (err, resp) {
        if (err) throw err;
        res.json(resp);
    });
});

productRouter.route('/:productId')
.get(function (req, res, next) {
    Product.findById(req.params.productId)
        .populate('comments.postedBy')
        .exec(function (err, product) {
        if (err) throw err;
        res.json(product);
    });
})

.put(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req, res, next) {
    Product.findByIdAndUpdate(req.params.productId, {
        $set: req.body
    }, {
        new: true
    }, function (err, product) {
        if (err) throw err;
        res.json(product);
    });
})

.delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req, res, next) {
        Product.findByIdAndRemove(req.params.productId, function (err, resp) {
        if (err) throw err;
        res.json(resp);
    });
});

productRouter.route('/:productId/comments')
.all(Verify.verifyOrdinaryUser)

.get(function (req, res, next) {
    Product.findById(req.params.productId)
        .populate('comments.postedBy')
        .exec(function (err, product) {
        if (err) throw err;
        res.json(product.comments);
    });
})

.post(function (req, res, next) {
    Product.findById(req.params.productId, function (err, product) {
        if (err) throw err;
        req.body.postedBy = req.decoded._doc._id;
        product.comments.push(req.body);
        product.save(function (err, product) {
            if (err) throw err;
            console.log('Updated Comments!');
            res.json(product);
        });
    });
})

.delete(Verify.verifyAdmin, function (req, res, next) {
    Product.findById(req.params.productId, function (err, product) {
        if (err) throw err;
        for (var i = (product.comments.length - 1); i >= 0; i--) {
            product.comments.id(product.comments[i]._id).remove();
        }
        product.save(function (err, result) {
            if (err) throw err;
            res.writeHead(200, {
                'Content-Type': 'text/plain'
            });
            res.end('Deleted all comments!');
        });
    });
});

productRouter.route('/:productId/comments/:commentId')
.all(Verify.verifyOrdinaryUser)

.get(function (req, res, next) {
    Product.findById(req.params.productId)
        .populate('comments.postedBy')
        .exec(function (err, product) {
        if (err) throw err;
        res.json(product.comments.id(req.params.commentId));
    });
})

.put(function (req, res, next) {
    // We delete the existing commment and insert the updated
    // comment as a new comment
    Product.findById(req.params.productId, function (err, product) {
        if (err) throw err;
        product.comments.id(req.params.commentId).remove();
                req.body.postedBy = req.decoded._doc._id;
        product.comments.push(req.body);
        product.save(function (err, product) {
            if (err) throw err;
            console.log('Updated Comments!');
            res.json(product);
        });
    });
})

.delete(function (req, res, next) {
    Product.findById(req.params.productId, function (err, product) {
        if (product.comments.id(req.params.commentId).postedBy
           != req.decoded._doc._id) {
            var err = new Error('You are not authorized to perform this operation!');
            err.status = 403;
            return next(err);
        }
        product.comments.id(req.params.commentId).remove();
        product.save(function (err, resp) {
            if (err) throw err;
            res.json(resp);
        });
    });
});

module.exports = productRouter;